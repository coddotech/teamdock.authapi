# Prepare publish files
FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# Copy everything
COPY . ./

# Restore dependencies and publish
RUN dotnet restore -v q
RUN dotnet publish -c Release src/TeamDock.AuthApi/ -o ../../publish_output -v q

# Build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app

COPY --from=build-env /app/publish_output/ .
ENTRYPOINT ["dotnet", "TeamDock.AuthApi.dll"]

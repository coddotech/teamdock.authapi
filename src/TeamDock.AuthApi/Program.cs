﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace TeamDock.AuthApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(opt =>
                {
                })
                .UseUrls("http://+:80")
                .Build();
    }
}
